/**
 * @file
 * An asynchronous, queue based parent menu item filter for Drupal's node form.
 * 
 * The parent menu item filter functionality is useful on sites where node forms
 * contain a huge number of menu items in the "Parent menu item" element. This
 * script, combined with the module file, makes the drop down element searchable
 * and makes it easier for content updaters to select the correct parent menu
 * item.
 * 
 * Finding relevant elements from the drop down is pretty straghtforward but
 * displaying the results in an easy-to-read way is far from it. The only way to
 * provide the user with enough context is to display the entire navigation path
 * per link. This requires traversing the navigation hierarchy from the flat
 * list provided by Drupal and is quite a daunting task to be done at once. This
 * script tackles the traversal challenge by queueing the found items in an
 * array and processing that array asynchronously. The traversal results are
 * cached in the items themselves and are only being processed once.
 * 
 * 
 * @author Kimmo Tapala / Verkkojulkaisut Oy  <kimmo.tapala@verkkojulkaisut.fi>
 */
;(function($){
	"use strict";
	
	// "Global" variables
	var _cache = {
			searchElement: null, // The search input element (the "menuQueue_finished" event is triggered on this element)
			allOptions: null, // All option elements in the menu parent item drop down
			resultContainer: null // Search result container
		},
		_optionQueue = [], // Queue of items to be processed
		_finishedItems = [], // Processed items
		_queueRunning = false, // Is the queue running
		_queueInterval = 10, // Interval at which the queue is processed
		_queueBlock = 10, // How meny items to process per _processQueue invocation
		_queueTimer = null, // Process interval timer
		_queueFinished = false; // Is the queue finsihed (only used to keep track on when to trigger the "menuQueue_finished" event)
	
	
	
	/**
	 * Queue processing function.
	 */
	function _processQueue(){
		if(_queueRunning){
			if(_optionQueue.length > 0){
				_queueFinished = false; // Queue contains items => it's not finished
				
				var currentItems = _optionQueue.slice(0, _queueBlock); // Get items to process during this invocation
				
				
				// Update queue for next invocation
				if(_optionQueue.length <= _queueBlock){
					_optionQueue = [];
				}
				else{
					_optionQueue = _optionQueue.slice(_queueBlock);
				}
				
				
				// Process current batch of items
				for(var i = 0; i < currentItems.length; i++){
					
					// Only traverse items that are not yet traversed
					if(typeof currentItems[i].data('nav_path') === 'undefined'){
						traverseUp(currentItems[i], _cache.allOptions);
					}
					
					_finishedItems.push(currentItems[i]); // We're done with this item - next, please!
				}
			}
			else{
				// Trigger custom event to inform that the queue is finished
				if(!_queueFinished && _cache.searchElement !== null){
					$(_cache.searchElement).trigger('menuQueue_finished');
				}
				
				_queueFinished = true; // Queue is empty => it's finished for now
			}
			
			_queueTimer = setTimeout(_processQueue, _queueInterval);
		}
	}
	
	
	
	
	
	
	/**
	 * Applies the search term filtering.
	 * 
	 * @param  string  term  Search term to use in filtering.
	 */
	function applyFilter(term){
		// Cache the drop down
		if(_cache.allOptions == null){
			_cache.allOptions = $('#edit-menu-parent option');
		}
		
		// Stop queue processing
		if(_queueTimer !== null){
			clearTimeout(_queueTimer);
			_queueRunning = false;
		}
		
		// Empty stacks
		_optionQueue = [];
		_finishedItems = [];
		
		
		var $options = _cache.allOptions,
			selectedOptions = [];
		
		// Find relevant items
		if(term !== ''){
			term = term.toLowerCase();
			
			$options.each(function(){
				var $option = $(this),
					value = '' + $option.text().toLowerCase();
				
				if(value !== '' && value.indexOf(term) >= 0){
					selectedOptions.push($option);
				}
			});
			
			// Populate queue with new items
			_optionQueue = selectedOptions;
			
			// Start queue processing
			if(!_queueRunning){
				_queueRunning = true;
				_processQueue();
			}
		}
	}
	
	
	
	
	
	
	/**
	 * Select a drop down option by value.
	 * 
	 * @param  string  selector  Selector string for select elements. (or elements)
	 * @param  string  value     Value to search for.
	 */
	function selectOptionByValue(selector, value){
		$(selector).each(function(){
			$(this).find('option').removeAttr('selected');
			$(this).find('option[value="' + value + '"]').attr('selected', true);
		});
	}
	
	
	
	
	
	
	/**
	 * Output search results.
	 * 
	 * @param  array  selectedOptions  An array of option elements to display.
	 */
	function appendResults(selectedOptions){
		var $list = $('<ul />');
		
		for(var i = 0; i < selectedOptions.length; i++){
			var $option = selectedOptions[i],
				$item = $('<li />'),
				$link = $('<a />');
			
			$link.text($option.data('nav_path')); // Display the traversed navigation path
			$link.attr('href', $option.val());
			
			$link.click(function(e){
				e.preventDefault();
				selectOptionByValue('#edit-menu-parent', $(this).attr('href'));
				_cache.resultContainer.empty();
			});
			
			
			$item.append($link);
			$list.append($item);
		}
		
		// Clear existing results and append new ones
		_cache.resultContainer.append($list);
	}
	
	
	
	
	
	
	
	/**
	 * The devil himself: item navigation path traversal.
	 * 
	 * @param  element   $option   A jQuery object representing the option element.
	 * @param  elements  $options  A jQuery object representing all option elements.
	 * 
	 * @return element             The original jQuery object representing the option element. ($option)
	 */
	function traverseUp($option, $options){
		var $preceding = [], // Option elements preceding $option
			$option_value = $option.val(),
			$option_text = $option.text();
		
		
		// Find preceding options
		$options.each(function(){
			var $item = $(this);
			
			$preceding[$preceding.length] = $item;
			
			// Stop iterating when $option is reached.
			if($item.val() === $option_value){
				return false;
			}
		});
		
		
		
		/**
		 * Options are iterated in reverse order. Therefore, the current item
		 * is set as the first one in the path stack.
		 */
		var path = ($option_text.indexOf('<') === 0) ? [] : [$option_text], // Add $option to current navigation path if it's not referencing a menu
			current = $option_text.indexOf(' '); // The current navigation depth is designated by the first space in the option label (yeah, I know...)
		
		for(var i = $preceding.length; i--;){
			var $preceding_item = $preceding[i],
				item_text = $preceding_item.text(),
				is_menu = (item_text.indexOf('<') === 0);
			
			if(is_menu || current > item_text.indexOf(' ')){
				path.push(item_text);
				current = $preceding_item;
			}
			
			// The option itself is a menu => no nesting
			if(is_menu){
				break;
			}
		}
		
		// Store navigation path in the item data to make it universally available.
		// Should we actually store the array, too?
		$option.data('nav_path', path.reverse().join(' » ').replace(/--/g, ''));
		
		return $option;
	}
	
	
	
	
	
	
	
	
	
	// Attach behavior
	Drupal.behaviors.vj_parent_search = {
		attach: function(context, settings){
			// Cache the search element to prevent unnecessary lookups
			_cache.searchElement = $('#edit-menu-parent-search', context);
			_cache.resultContainer = $('#menu-parent-search-results');
			
			_cache.searchElement.once('vjparentsearch', function(){
				var searchTimer = null;
				
				// Bind search functionality to the keyup event of the input field
				_cache.searchElement.bind('keyup', function(e){
					var $element = $(this);
					
					if(searchTimer !== null){
						clearTimeout(searchTimer);
					}
					
					if($element.val().length > 2){
						searchTimer = setTimeout(function(){
							_cache.resultContainer.empty();
							_cache.resultContainer.addClass('processing');
							applyFilter($element.val());
						}, 1000);
					}
				});
				
				// Display search results when search is finished
				_cache.searchElement.bind('menuQueue_finished', function(){
					_cache.resultContainer.removeClass('processing');
					appendResults(_finishedItems);
				});
			});
		}
	};
})(jQuery);
